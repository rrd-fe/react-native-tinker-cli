"use strict";
/**
 * 发版新版本
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inquirer = __importStar(require("inquirer"));
const path = require('path');
const fs = require('fs');
const fsDiff = require('fs-diff');
const walk = require('ignore-walk');
const env_reader_1 = __importDefault(require("../middleware/env-reader"));
const app_1 = __importDefault(require("../service/app"));
const fs_helper_1 = __importDefault(require("../utils/fs-helper"));
exports.command = 'publish';
exports.desc = '上传RN源代码发版';
function builder(argv) {
    return argv.option('env', {
        type: 'string',
        desc: '环境名',
        demandOption: true,
    }).option('appKey', {
        type: 'string',
        desc: '要发版的app key',
        demandOption: true,
    }).option('appVersion', {
        type: 'string',
        desc: '对应的native版本号',
        demandOption: true,
    }).option('desc', {
        type: 'string',
        desc: '本次发版的描述',
        demandOption: true,
    }).middleware([env_reader_1.default]);
}
exports.builder = builder;
function handler(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const envObj = args.$envObj;
        if (!envObj.getNoahToken()) {
            return console.error(`未登录`);
        }
        const appService = new app_1.default(envObj);
        let result = null;
        try {
            result = yield appService.relateApps();
        }
        catch (err) {
            return console.error(`获取APP列表异常：${err.message}`);
        }
        //找到对应APP的名字
        const appList = [].concat(result.own, result.write);
        const targetApp = appList.find((obj) => {
            return obj.appKey === args.appKey;
        });
        if (!targetApp) {
            return console.error(`appKey[${args.appKey}]对应的APP不存在，或者没有权限`);
        }
        const out = yield inquirer.prompt([
            {
                type: 'confirm',
                name: 'isConfirm',
                default: false,
                message: `再次确认发版信息： 发版环境[${args.env}] app名[${targetApp.name}] native版本号[${args.appVersion}]`
            }
        ]);
        if (!out.isConfirm) {
            return;
        }
        const cwd = process.cwd();
        const tempDir = yield fs_helper_1.default.generateTempDir('tinker');
        const zipFilePath = path.normalize(`${tempDir}${path.sep}rn_source.zip`);
        console.log(`产出的临时zip文件路径为：${zipFilePath}`);
        let files = [];
        //根据源码中配置的 .tinkerignore 文件，来过滤不需要上传的的文件/目录
        try {
            files = walk.sync({
                path: cwd,
                ignoreFiles: ['.tinkerignore'],
                includeEmpty: true,
                follow: false // true to follow symlink dirs, default false
            });
            files = files.map((sub) => {
                return path.join(`${cwd}${path.sep}${sub}`);
            });
            // files = <string[]> await fsDiff.read.getFiles(cwd, `${cwd}${path.sep}`, (filePath: string) => {
            //     if( filePath.indexOf('node_modules') === 0 ){
            //         return false;
            //     }
            //     if( filePath.indexOf('.tinker.') === 0 ){
            //         return false;
            //     }
            //     if( filePath.includes('.git') || filePath.includes('.idea') ){
            //         return false;
            //     }
            //     return true;
            // });
        }
        catch (err) {
            return console.error(`读取源文件列表异常！${err.message}`);
        }
        console.log(`要打包的源文件列表：\n`, files.join('\n'));
        const prefix = `${cwd}${path.sep}`;
        try {
            yield fs_helper_1.default.zipFiles(files, zipFilePath, prefix);
        }
        catch (err) {
            return console.error(`生成源代码zip包异常！${err.message}`);
        }
        console.log(`保存zip源码包成功: ${zipFilePath}`);
        let publishResult = null;
        try {
            publishResult = yield appService.publishBySource({
                sourceZipFile: zipFilePath,
                appKey: args.appKey,
                appVersion: args.appVersion,
                desc: args.desc,
            });
        }
        catch (err) {
            return console.error(`调用发布接口异常！${err.message}`);
        }
        //清除中间临时文件
        try {
            fs.unlinkSync(zipFilePath);
        }
        catch (err) {
            console.warn(`清除临时zip文件失败！文件路径：\n ${zipFilePath} \n 错误信息：${err.message}`);
        }
        console.info(`成功发版！本次发版任务：${publishResult.taskId}`);
        console.info(`本次发版任务详情URL：\n`);
        const taskUrl = args.$envObj.resolveUrl(`/dash/tasks/detail?appId=${targetApp.id}&taskId=${publishResult.taskId}`);
        console.info(`${taskUrl}\n`);
    });
}
exports.handler = handler;
