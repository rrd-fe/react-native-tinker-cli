"use strict";
/**
 * 输出cli使用帮助文档
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.command = `test`;
exports.aliases = ['test', 't'];
exports.desc = `tinker demo`;
function builder(argv) {
    argv.reset();
    return argv.middleware([test]);
}
exports.builder = builder;
function handler(argv) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log(`begin tinker: `, argv._test);
        const out = yield new Promise((resolve, reject) => {
            console.log(`in promise`);
            setTimeout(() => {
                console.log('before resolve');
                resolve('hi');
            }, 1000);
        });
        console.log('after promise, reulst: ' + out);
    });
}
exports.handler = handler;
function test(argv) {
    console.log('in middleware test');
    // const out = await new Promise<string>( (resolve, reject) => {
    //     setTimeout( () => {
    //         console.log('async middleware before resolve');
    //         resolve('async middleware');
    //     }, 2000);
    // });
    argv._test = 'tet';
}
