"use strict";
/**
 * 当前登录用户有权限的APP列表
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cli_table_1 = __importDefault(require("cli-table"));
const env_reader_1 = __importDefault(require("../middleware/env-reader"));
const app_1 = __importDefault(require("../service/app"));
exports.command = 'apps';
exports.desc = '查看有权限的APP列表';
function builder(argv) {
    return argv.option('env', {
        type: 'string',
        desc: '环境名',
        demandOption: true,
    }).middleware([env_reader_1.default]);
}
exports.builder = builder;
function handler(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const appService = new app_1.default(args.$envObj);
        let result = null;
        try {
            result = yield appService.relateApps();
        }
        catch (err) {
            return console.error(`获取APP列表异常：${err.message}`);
        }
        const table = new cli_table_1.default({
            head: ['id', 'appKey', '所属平台', 'APP简介', 'git仓库地址', '打包入口脚本', '产出bundle文件名']
        });
        const arr = [].concat(result.own, result.write);
        arr.forEach((obj) => {
            table.push([
                obj.id,
                obj.appKey,
                obj.platformName,
                obj.desc,
                obj.gitUrl,
                obj.entryFile,
                obj.bundleName
            ]);
        });
        console.log(table.toString());
    });
}
exports.handler = handler;
