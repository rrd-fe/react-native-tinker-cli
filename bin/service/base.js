"use strict";
/**
 * service base class
 */
Object.defineProperty(exports, "__esModule", { value: true });
class BaseService {
    constructor(env) {
        this.env = env;
    }
    resolveUrl(urlPath) {
        return this.env.getServer() + urlPath;
    }
    getNoahToken() {
        return this.env.getNoahToken();
    }
}
exports.default = BaseService;
