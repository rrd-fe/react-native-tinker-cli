/**
 * 创建一个env
 */

import * as yargs from 'yargs';
const fs = require('fs');
import { Env } from '../../definitions';

interface IArgs extends yargs.Arguments{
    name: string;
    server: string;
}

export const command = 'create';
export const desc = '创建环境';
export function builder(argv: yargs.Argv): yargs.Argv{
    return argv.option('name', {
        desc: '要创建的环境名，请使用【字母、数字】组合',
        demandOption: true,
        type: 'string',
    }).option('server', {
        desc: '该环境对应的server信息，示例：http://test.tinker.com',
        demandOption: true,
        type: 'string',
        string: true,
    });
}
export async function handler(argv: IArgs){
    if( Env.isEnvExist(argv.name) ){
        console.error(`已经存在相同的环境名了: ${argv.name}`);
        return;
    }
    if( ! argv.server ){
        return console.error(`[server]不能为空`);
    }
    const filePath = Env.resolveEnvFile(argv.name);
    try{
        const conf = {
            server: argv.server
        };
        fs.writeFileSync(filePath, JSON.stringify(conf));
    }catch(err){
        console.error(`写入环境配置文件失败`);
        return;
    }
    console.info(`创建环境成功`);
}