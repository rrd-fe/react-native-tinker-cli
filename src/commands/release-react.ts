/**
 * 通过在本地调用  react-native bundle 命令打包产出全量包，再上传到tinker-server方式发版
 */

import * as yargs from 'yargs';
import * as inquirer from 'inquirer';
import Table from 'cli-table';
const path = require('path');
const fs = require('fs');
const childProcess = require('child_process');
const fse = require('fs-extra');
const fsDiff = require('fs-diff');
import AdmZip from 'adm-zip';
import chalk from 'chalk';
const walk = require('ignore-walk');
import envLoader from '../middleware/env-reader';
import { IBaseArgs, IApp, PlatformText } from '../definitions/index';
import AppService, { IPublishResult } from '../service/app';
import FSHelper from '../utils/fs-helper';
import * as commandUtil from '../utils/command';

interface IArgs extends IBaseArgs{
    appKey?: string;
    appVersion?: string;
    desc?: string;

    //下面都是透传给 react-native bundle 的参数
    entryFile?: string;
    sourcemapOutput?: string;
    config?: string;
}

export const command = 'release-react';
export const desc = '上传本地产出bundle发版';
export function builder(argv: yargs.Argv){
    return argv.option('env', {
        type: 'string',
        desc: '环境名',
        demandOption: true,
      }).option('appKey', {
        type: 'string',
        desc: '要发版的app key',
        demandOption: true,
    }).option('appVersion', {
        type: 'string',
        desc: '对应的native版本号',
        demandOption: true,
    }).option('desc', {
        type: 'string',
        desc: '本次发版的描述',
        demandOption: true,
    }).option('entryFile', {
        type: 'string',
        desc: '打包入口文件',
        demandOption: false,
    }).option('sourcemapOutput', {
        type: 'string',
        desc: '产出包的 source-map',
        demandOption: false,
    }).option('config', {
        type: 'string',
        desc: 'react-native bundle config参数',
        demandOption: false,
    }).middleware([envLoader]);
}

export async function handler(args: IArgs){
    const envObj = args.$envObj!;
    if( ! envObj.getNoahToken() ){
        return console.error(`未登录`);
    }

    const appService = new AppService(envObj);
    let result = null;
    try{
        result = await appService.relateApps();
    }catch(err){
        return console.error(`获取APP列表异常：${err.message}`);
    }

    //找到对应APP的名字
    const appList = (<IApp[]>[]).concat(result.own, result.write);
    const targetApp = appList.find( (obj) => {
        return obj.appKey === args.appKey;
    });

    if( ! targetApp ){
        return console.error(`appKey[${args.appKey}]对应的APP不存在，或者没有权限`);
    }

    const out = <inquirer.Answers> await inquirer.prompt([
        {
            type: 'confirm',
            name: 'isConfirm',
            default: false,
            message: `再次确认[bundle]发版信息： 发版环境[${args.env}] app名[${targetApp.name}] native版本号[${args.appVersion}]`
        }
    ]);
    if( ! out.isConfirm ){
        return;
    }

    const cwd = process.cwd();
    const tempDir = await FSHelper.generateTempDir('tinker_release');
    let entryFile = args.entryFile;
    const platformString = PlatformText[targetApp.platform].toLocaleLowerCase();
    if( ! entryFile ){
        entryFile = `index.${platformString}.js`;
        console.info(`未置顶 entryFile ，按照平台自动设置为：${entryFile}`);
    }

    try{
        console.log(`react-native bundle产出目录：${tempDir}`);
        await runReactNativeBundle(targetApp.bundleName, false, entryFile, tempDir, platformString, args.sourcemapOutput, args.config);
    }catch(err){
        return commandUtil.fail(err);
    }

    const zipDir = await FSHelper.generateTempDir('tinker_zip');
    const zipFilePath = path.normalize(`${zipDir}${path.sep}rinker_package.zip`);
    console.log(`准备生成zip包，产出的临时zip文件路径为：${zipFilePath}`);

    try{
        await FSHelper.zipDir(tempDir, zipFilePath);
    }catch(err){
        return commandUtil.fail(err);
    }

    let publishResult: IPublishResult | null = null;
    try{
        publishResult = await appService.publishByBundle({
            zipFile: zipFilePath, 
            appKey: args.appKey!,
            appVersion: args.appVersion!,
            desc: args.desc!,
        });
    }catch(err){
        return console.error(`调用发布接口异常！${err.message}`);
    }

    //清除中间临时文件
    try{
        fse.removeSync(tempDir);
        fse.removeSync(zipDir);
        fse.removeSync(zipFilePath);
    }catch(err){
        console.warn(`清除临时文件失败 \n 错误信息：${err.message}`);
    }

    console.info(`成功发版！本次发版任务：${publishResult!.taskId}`);
    console.info(`本次发版任务详情URL：\n`);
    const taskUrl = args.$envObj!.resolveUrl(`/dash/tasks/detail?appId=${targetApp!.id}&taskId=${publishResult!.taskId}`);
    console.info(`${taskUrl}\n`);

}

/**
 * 代码来自MS的 code-push: 
 * https://github.com/Microsoft/code-push/blob/064d70ddaff16b131836ece3723505e01844b5fa/cli/script/command-executor.ts#L1442
 * @param bundleName 
 * @param development 
 * @param entryFile 
 * @param outputFolder 
 * @param platform 
 * @param sourcemapOutput 
 * @param config 
 */
function runReactNativeBundle(bundleName: string, development: boolean, entryFile: string, outputFolder: string, platform: string, sourcemapOutput?: string, config?: string): Promise<void>{
    let reactNativeBundleArgs: string[] = [];

    Array.prototype.push.apply(reactNativeBundleArgs, [
        path.join("node_modules", "react-native", "local-cli", "cli.js"), "bundle",
        "--assets-dest", outputFolder,
        "--bundle-output", path.join(outputFolder, bundleName),
        "--dev", development,
        "--entry-file", entryFile,
        "--platform", platform,
    ]);

    if (sourcemapOutput) {
        reactNativeBundleArgs.push("--sourcemap-output", sourcemapOutput);
    }

    if (config) {
        reactNativeBundleArgs.push("--config", config);
    }

    console.log(chalk.cyan("Running \"react-native bundle\" command:\n"));
    var reactNativeBundleProcess = childProcess.spawn("node", reactNativeBundleArgs);
    console.log(`node ${reactNativeBundleArgs.join(" ")}`);

    return new Promise<void>((resolve, reject) => {
        reactNativeBundleProcess.stdout.on("data", (data: Buffer) => {
            console.log(data.toString().trim());
        });

        reactNativeBundleProcess.stderr.on("data", (data: Buffer) => {
            console.error(data.toString().trim());
        });

        reactNativeBundleProcess.on("close", (exitCode: number) => {
            if (exitCode) {
                return reject(new Error(`"react-native bundle" command exited with code ${exitCode}.`));
            }
            resolve();
        });
    });
}