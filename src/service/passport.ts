/**
 * 用户登录、登出相关接口
 */

import * as request from 'superagent';
import BaseService from './base';
import { NOAH_TOKEN_NAME } from '../definitions';

export interface ILoginParams{
    userName: string;
    password: string;
}

export interface ILoginOutput{
    token: string;
    userName: string;
}

export default class PassportService extends BaseService{

    /**
     * 登录接口
     * @param data 
     */
    async login(data: ILoginParams): Promise<ILoginOutput>{
        const url = this.resolveUrl(`/dash/cli/login`);
        // console.log(`login url: ${url}`);
        const res = await request.post(url).send(data);
        const body = res.body;
        if(body.status === 0){
            const token = res.header[NOAH_TOKEN_NAME];
            const userName = res.body.name;
            return {
                token,
                userName,
            };
        }else{
            throw new Error(body.message);
        }
    }
}