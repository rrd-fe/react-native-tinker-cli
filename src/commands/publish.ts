/**
 * 发版新版本
 */

import * as yargs from 'yargs';
import * as inquirer from 'inquirer';
import Table from 'cli-table';
const path = require('path');
const fs = require('fs');
const fsDiff = require('fs-diff');
import AdmZip from 'adm-zip';
const walk = require('ignore-walk');
import envLoader from '../middleware/env-reader';
import { IBaseArgs, IApp } from '../definitions/index';
import AppService, { IPublishResult } from '../service/app';
import FSHelper from '../utils/fs-helper';

interface IArgs extends IBaseArgs{
    appKey?: string;
    appVersion?: string;
    desc?: string;
}

export const command = 'publish';
export const desc = '上传RN源代码发版';
export function builder(argv: yargs.Argv){
    return argv.option('env', {
        type: 'string',
        desc: '环境名',
        demandOption: true,
      }).option('appKey', {
        type: 'string',
        desc: '要发版的app key',
        demandOption: true,
    }).option('appVersion', {
        type: 'string',
        desc: '对应的native版本号',
        demandOption: true,
    }).option('desc', {
        type: 'string',
        desc: '本次发版的描述',
        demandOption: true,
    }).middleware([envLoader]);
}
export async function handler(args: IArgs){

    const envObj = args.$envObj!;
    if( ! envObj.getNoahToken() ){
        return console.error(`未登录`);
    }

    const appService = new AppService(envObj);
    let result = null;
    try{
        result = await appService.relateApps();
    }catch(err){
        return console.error(`获取APP列表异常：${err.message}`);
    }

    //找到对应APP的名字
    const appList = (<IApp[]>[]).concat(result.own, result.write);
    const targetApp = appList.find( (obj) => {
        return obj.appKey === args.appKey;
    });

    if( ! targetApp ){
        return console.error(`appKey[${args.appKey}]对应的APP不存在，或者没有权限`);
    }

    const out = <inquirer.Answers> await inquirer.prompt([
        {
            type: 'confirm',
            name: 'isConfirm',
            default: false,
            message: `再次确认发版信息： 发版环境[${args.env}] app名[${targetApp.name}] native版本号[${args.appVersion}]`
        }
    ]);
    if( ! out.isConfirm ){
        return;
    }

    const cwd = process.cwd();
    const tempDir = await FSHelper.generateTempDir('tinker');
    const zipFilePath = path.normalize(`${tempDir}${path.sep}rn_source.zip`);
    console.log(`产出的临时zip文件路径为：${zipFilePath}`);

    let files = [];

    //根据源码中配置的 .tinkerignore 文件，来过滤不需要上传的的文件/目录
    try{
        files = walk.sync({
            path: cwd,
            ignoreFiles: [ '.tinkerignore' ], // list of filenames. defaults to ['.ignore']
            includeEmpty: true, // true to include empty dirs, default false
            follow: false // true to follow symlink dirs, default false
        });

        files = files.map( (sub: string) => {
            return path.join(`${cwd}${path.sep}${sub}`);
        });

        // files = <string[]> await fsDiff.read.getFiles(cwd, `${cwd}${path.sep}`, (filePath: string) => {
        //     if( filePath.indexOf('node_modules') === 0 ){
        //         return false;
        //     }
        //     if( filePath.indexOf('.tinker.') === 0 ){
        //         return false;
        //     }
        //     if( filePath.includes('.git') || filePath.includes('.idea') ){
        //         return false;
        //     }
        //     return true;
        // });
    }catch(err){
        return console.error(`读取源文件列表异常！${err.message}`);
    }

    console.log(`要打包的源文件列表：\n`, files.join('\n'));
    const prefix = `${cwd}${path.sep}`;
    try{
        await FSHelper.zipFiles(files, zipFilePath, prefix);
    }catch(err){
        return console.error(`生成源代码zip包异常！${err.message}`);
    }
    console.log(`保存zip源码包成功: ${zipFilePath}`);

    let publishResult: IPublishResult | null = null;
    try{
        publishResult = await appService.publishBySource({
            sourceZipFile: zipFilePath, 
            appKey: args.appKey!,
            appVersion: args.appVersion!,
            desc: args.desc!,
        });
    }catch(err){
        return console.error(`调用发布接口异常！${err.message}`);
    }

    //清除中间临时文件
    try{
        fs.unlinkSync(zipFilePath);
    }catch(err){
        console.warn(`清除临时zip文件失败！文件路径：\n ${zipFilePath} \n 错误信息：${err.message}`);
    }

    console.info(`成功发版！本次发版任务：${publishResult!.taskId}`);
    console.info(`本次发版任务详情URL：\n`);
    const taskUrl = args.$envObj!.resolveUrl(`/dash/tasks/detail?appId=${targetApp!.id}&taskId=${publishResult!.taskId}`);
    console.info(`${taskUrl}\n`);
}