/**
 * 创建一个 env 的command
 */

import yargs = require("yargs");

export const command = 'env <command>';
export const desc = '环境相关';
export function builder(sub: yargs.Argv){
    return sub.commandDir('env-commands');
}
// export async function handler(argv: yargs.Arguments){

// }