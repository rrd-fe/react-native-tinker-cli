/**
 * 下载某个APP，某个native版本对应的最新全量包
 */

import * as yargs from 'yargs';
import * as inquirer from 'inquirer';
const path = require('path');
const fs = require('fs');
const fse = require('fs-extra');
const AdmZip = require('adm-zip');
import envLoader from '../middleware/env-reader';
import { IBaseArgs, IApp, PlatformText, AppPlatform } from '../definitions/index';
import AppService, { IPartialApp } from '../service/app';
import * as commandUtil from '../utils/command';
import FSHelper from '../utils/fs-helper';
import { platform } from 'os';

interface IArgs extends IBaseArgs{
    appKey?: string;
    appVersion?: string;
    confirm?: boolean;
    //ios下，
    iosDest?: string;
    //android下，assets目录
    androidAssets?: string;
    //android下，图片资源要拷贝到的res目录
    androidRes?: string;
}

export const command = 'download';
export const desc = '下载最新的RN全量包';
export function builder(argv: yargs.Argv){
    return argv.option('env', {
        type: 'string',
        desc: '环境名',
        demandOption: true,
      }).option('appKey', {
        type: 'string',
        desc: 'app对应的key',
        demandOption: true,
    }).option('appVersion', {
        type: 'string',
        desc: '对应的native版本号',
        demandOption: true,
    }).option('confirm', {
        type: 'boolean',
        desc: '确认发版',
    }).option('iosDest', {
        type: 'string',
        desc: 'iOS APP解压全量包的目录',
    }).option('androidAssets', {
        type: 'string',
        desc: 'Android APP解压的assets目录',
    }).option('androidRes', {
        type: 'string',
        desc: 'Android APP解压res的目录',
    }).middleware([envLoader]);
}
export async function handler(args: IArgs){
    const appService = new AppService(args.$envObj!);

    let app = null;
    try{
        app = await appService.getPartialAppByKey(args.appKey!);
    }catch(err){
        return commandUtil.fail( new Error(`查询appKey[${args.appKey}]对应的应用详情异常！${err.message}`));
    }

    if( ! args.confirm ){
        //命令行没有强制确认，需要用户手动确认
        const out = <inquirer.Answers> await inquirer.prompt([
            {
                type: 'confirm',
                name: 'isConfirm',
                default: false,
                message: `再次确认下载全量包信息： 环境[${args.env}] app名[${app.name}] platform[${PlatformText[app.platform]}] native版本号[${args.appVersion}]`
            }
        ]);
        if( ! out.isConfirm ){
            return;
        }
    }

    if( app.platform === AppPlatform.ios ){
        if( ! args.iosDest ){
            return commandUtil.fail( new Error(`iOS的APP发版，--iosDest 解压目录必传！`));
        }
        args.iosDest = path.resolve(process.cwd(), args.iosDest);
        fse.ensureDirSync(args.iosDest);
    }
    if( app.platform === AppPlatform.android ){
        if( ! args.androidAssets || ! args.androidRes ){
            return commandUtil.fail( new Error(`Android的APP发版，--androidAssets --androidRes 解压目录必传！`));
        }
        args.androidAssets = path.resolve(process.cwd(), args.androidAssets);
        fse.ensureDirSync(args.androidAssets);
        args.androidRes = path.resolve(process.cwd(), args.androidRes);
        fse.ensureDirSync(args.androidRes);
    }

    let localZipFile = '';
    try{
        const tempDir = await FSHelper.generateTempDir('tinker');
        localZipFile = path.normalize(`${tempDir}${path.sep}download_package.zip`);
    }catch(err){
        return commandUtil.fail( new Error(`生成临时zip包下载文件失败！${err.message}`));
    }

    console.log(`全量zip包临时下载文件路径：${localZipFile}`);

    try{
        await appService.downloadLatestPackage({
            appKey: args.appKey!,
            appVersion: args.appVersion!,
            saveFilePath: localZipFile
        });
    }catch(err){
        return commandUtil.fail( new Error(`下载全量包异常！${err.message}`) );
    }

    try{
        switch(app.platform){
            case AppPlatform.android:
                await androidHandle(app, args, localZipFile);
                break;
            case AppPlatform.ios:
                await iosHandle(app, args, localZipFile);
                break;
            default:
                throw new Error(`不支持的APP平台：${app.platform}`);
        }
    }catch(err){
        return commandUtil.fail(err);
    }

    //清除临时的zip包
    try{
        fs.unlinkSync(localZipFile);
        console.info(`临时全量包删除完成`);
    }catch(err){
        console.error(`删除临时zip全量包异常！[${localZipFile}] ${err.message}`);
    }

    commandUtil.succeed(`下载全量包成功`);
}


/**
 * iOS下，只需要把全量包解压到指定的一个目录，就O了
 * @param app 
 * @param args 
 * @param zipFilePath 
 */
async function iosHandle(app: IPartialApp, args: IArgs, zipFilePath: string): Promise<void>{
    await FSHelper.unzip2Dir(zipFilePath, args.iosDest!, true);
}

/**
 * Android下，图片资源和代码资源(以及后台系统生成的hot.json等文件)，需要解压到2个不同的目录下
 * 代码资源是解压到 指定的assets 目录下
 * 图片资源是解压到 指定的res 目录下，对应的 drawable drawable-xxhdpi 等子目录下
 * @param app 
 * @param args 
 * @param zipFilePath 
 */
async function androidHandle(app: IPartialApp, args: IArgs, zipFilePath: string): Promise<void>{
    const zip = new AdmZip(zipFilePath);
    //先全部解压到 res 目录下，然后把代码mv 到 assets 目录
    await FSHelper.unzip2Dir(zipFilePath, args.androidRes!, true);
    const entries = zip.getEntries();
    const files: string[] = [];
    entries.forEach( (zipEntry: any) => {
        files.push(zipEntry.entryName);
        if(! /^\/?drawable/.test(zipEntry.entryName)){
            //move到 assets 目录
            const src = path.resolve(args.androidRes, zipEntry.entryName);
            const target = path.resolve(args.androidAssets, zipEntry.entryName);
            console.info(`从res目录移动文件(目录)到assets目录：src[${src}] target[${target}]`);
            fse.moveSync(src, target, true);
        }
    });
    const str = files.join('\n');
    const saveFilePath = path.normalize(`${args.androidAssets}/rn_files.txt`);
    fs.writeFileSync(saveFilePath, str, { encoding: 'utf8'});
}