"use strict";
/**
 * 创建一个 env 的command
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.command = 'env <command>';
exports.desc = '环境相关';
function builder(sub) {
    return sub.commandDir('env-commands');
}
exports.builder = builder;
// export async function handler(argv: yargs.Arguments){
// }
