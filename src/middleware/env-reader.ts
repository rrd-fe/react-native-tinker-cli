/**
 * 根据用户传的 env 变量，读取该 env 对应的配置
 */

const fs = require('fs');
import { IBaseArgs, Env } from "../definitions";
import yargs = require("yargs");

export default function envLoader(args: yargs.Arguments){
    
    args = <IBaseArgs> args;
    if( ! args.env ){
        throw new Error('env参数不能为空');
    }
    const obj = new Env(args.env);
    obj.loadFile();
    obj.loadCredential();
    args.$envObj = obj;
}