# react-native-tinker-cli

`React Native`热更新系统的配套发版命令行，配合 [tinker-system](https://github.com/rrd-fe/tinker-system) 使用

用户直接在本地的`react native`源代码目录中，通过本命令可以和 `tinker-system`后台交互，执行基本的登录、登出、发版等功能。

**注意** ：`tinker`命令，都 **必须** 在你的 **RN源代码根目录执行**！！

## 安装

```shell
$ npm i -g react-native-tinker-cli
```

## 环境配置

使用 `tinker-system`时，推荐的方式是，开发/测试使用的热更新版本，只部署在公司内网；生产环境，单独部署一份线上服务。这样会有一个问题，调用`cli`命令时，都会涉及到`cli`是和哪一个后台系统交互。因此，我们在 `cli` 里，加了个所谓的 **环境(env)** ，其实就是 1个环境，对应1个 `.json` 配置文件，放在 **当前命令执行的目录** 下，在调用 `cli` 时，明确指定是在和哪个 `env` 交互。

某个环境的配置文件名，规则如下：`.tinker.${envName}.json`

单个环境配置如下：

```json
{
    "server": "http://tinker.somecompany.com"
}
```

## 创建环境

上面环境配置文件，**不**需要手动创建，可以通过 tinker 命令来创建

```shell
tinker env create --name ${envName} --server "${envServer}"
# 示例
tinker env create --name test --server "http://test.tinker.com"
```

## 登录

```shell
tinker login --env ${envName}
```

## 登出

```shell
tinker logout --env ${envName}
```

## 查看有写权限的APP列表

```shell
tinker apps --env ${envName}
```


## 发布新版本(本地打包)【推荐】

这种发版方式，会先在用户本地电脑上，调用 `react-native/local-cli/cli.js` 打包之后，上传产出bundle到 tinker系统。`Code-Push`也是这种方式。

```shell
tinker release-react --env ${envName} --appKey ${appKey} --appVersion "${native对应版本}" --desc "本次发版描述" --entryFile "打包入口文件"
# 示例
tinker release-react  --env production --appKey fwq-1036-aaaa-89cd-qa --appVersion "4.0.0" --desc "发布4.0第一版测试" --entryFile "src/entry/index.android.js"
```


## 发布新版本(上传源码方式)

这种发版方式，会将 **RN源代码** 上传到tinker服务器上，由tinker系统负责在服务器上完成打包，产出bundle。

```shell
tinker publish --env ${envName} --appKey ${appKey} --appVersion "${native对应版本}" --desc "本次发版描述"
```

**注意**： 发版后，请登录后台系统，查看本次发版任务是否成功。在任务成功的情况下，目前，默认全量包，**不会** 对外开放，防止由于误操作或代码bug等，引起线上问题。建议 *线上* 环境，先通过 `abTest` 配置项，灰度发布，确认没问题之后，再打开全量发布。

目前发版，是在本地RN源代码根目录，将源码打包之后，发送给Noah后台，因此，需要指定，本地源码目录里，那些文件、目录是需要忽略的，通过配置 `.tinkerignore` 文件，来指定具体规则，配置语法同 `.gitignore`，下面是一个`.tinkerignore`文件内容示例：

```.gitignore
node_modules
dist
.idea
npm-debug.log
.git
.tinker.*.json
doc
README.md
```

## 下载最新全量包

`iOS` 和 `Android` 下，传的参数不同。

* iOS 下：`tinker download --env ${envName} --appKey "${appKey}" --appVersion "${native对应版本}" --iosDest "全量包解压后目录"`
* Android 下：`tinker download --env ${envName} --appKey "${appKey}" --appVersion "native对应版本" --androidAssets "assets目录，存放代码bundle" --androidRes "res目录，存放图片资源"`

示例：

```shell
# iOS 下
tinker download --env local --appKey "63445762-3aed-4c44-836a-ad9b16e3aae7" --appVersion "5.7.2"  --iosDest "./ios_output"

# Android 下
tinker download --env local --appKey "63445762-3aed-4c44-836a-ad9b16e3aae7" --appVersion "5.7.2" --androidAssets "./temp_assets/" --androidRes "./temp_res/" 
```

上面的命令，都会让用户再次确认，要下载的APP信息和native版本，是否OK；如果要跳过确认步骤，可以在命令行中加上 `--confirm` 参数

