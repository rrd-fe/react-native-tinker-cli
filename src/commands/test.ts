/**
 * 输出cli使用帮助文档
 */

import * as yargs from 'yargs';

export const command = `test`;
export const aliases = [ 'test', 't'];
export const desc = `tinker demo`;
export function builder(argv: yargs.Argv){
    argv.reset();
    return argv.middleware([test]);
}
export async function handler(argv: IArgs){
    console.log(`begin tinker: `, argv._test);
    const out = await new Promise((resolve, reject) => {
        console.log(`in promise`);
        setTimeout(() => {
            console.log('before resolve');
            resolve('hi');
        }, 1000);
    });
    console.log('after promise, reulst: ' + out);
}
// export const middlewares = [ test ];

interface IArgs extends yargs.Arguments{
    _test: string;
}

function test(argv: yargs.Arguments): void{
    console.log('in middleware test');
    // const out = await new Promise<string>( (resolve, reject) => {
    //     setTimeout( () => {
    //         console.log('async middleware before resolve');
    //         resolve('async middleware');
    //     }, 2000);
    // });
    argv._test = 'tet';
}