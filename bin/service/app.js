"use strict";
/**
 * 应用相关api
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = __importStar(require("superagent"));
const fs = require('fs');
const fsDiff = require('fs-diff');
const base_1 = __importDefault(require("./base"));
const definitions_1 = require("../definitions");
class AppService extends base_1.default {
    /**
     * 获取用户有 写 权限的APP列表
     */
    relateApps() {
        return __awaiter(this, void 0, void 0, function* () {
            const url = this.resolveUrl(`/dash/cli/relateApps`);
            const res = yield request.get(url).set(definitions_1.NOAH_TOKEN_NAME, this.getNoahToken());
            const body = res.body;
            if (body.status === 0) {
                return body.data;
            }
            else {
                throw new Error(body.message);
            }
        });
    }
    /**
     * 上传源代码压缩包发版
     * @param data
     */
    publishBySource(data) {
        return __awaiter(this, void 0, void 0, function* () {
            let md5 = '';
            try {
                md5 = yield fsDiff.sign.fileMd5(data.sourceZipFile);
            }
            catch (err) {
                throw new Error(`计算源代码包的md5异常！${err.message}`);
            }
            const url = this.resolveUrl(`/dash/cli/publishBySource`);
            const res = yield request.post(url)
                .set(definitions_1.NOAH_TOKEN_NAME, this.getNoahToken())
                .field('appKey', data.appKey)
                .field('appVersion', data.appVersion)
                .field('md5', md5)
                .field('desc', data.desc)
                .attach('sourceFile', data.sourceZipFile);
            const body = res.body;
            if (body.status === 0) {
                return body.data;
            }
            else {
                throw new Error(body.message);
            }
        });
    }
    /**
     * 在本地打好 bundle 之后，上传 bundle之后的全量包发版
     * @param data
     */
    publishByBundle(data) {
        return __awaiter(this, void 0, void 0, function* () {
            let md5 = '';
            try {
                md5 = yield fsDiff.sign.fileMd5(data.zipFile);
            }
            catch (err) {
                throw new Error(`计算源代码包的md5异常！${err.message}`);
            }
            const url = this.resolveUrl(`/dash/cli/publishByBundlePackage`);
            const res = yield request.post(url)
                .set(definitions_1.NOAH_TOKEN_NAME, this.getNoahToken())
                .field('appKey', data.appKey)
                .field('appVersion', data.appVersion)
                .field('md5', md5)
                .field('desc', data.desc)
                .attach('zipFile', data.zipFile);
            const body = res.body;
            if (body.status === 0) {
                return body.data;
            }
            else {
                throw new Error(body.message);
            }
        });
    }
    /**
     * 根据 appKey ，获取APP的部分公开属性
     * @param appKey 创建APP时，生成的key
     */
    getPartialAppByKey(appKey) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = this.resolveUrl(`/dash/cli/appDetail`);
            const res = yield request.get(url).set(definitions_1.NOAH_TOKEN_NAME, this.getNoahToken()).query({
                appKey,
            });
            const body = res.body;
            if (body.status === 0) {
                return body.data;
            }
            else {
                throw new Error(body.message);
            }
        });
    }
    /**
     * 下载某个APP下，某个native版本对应的最新的RN全量包
     * @param data
     */
    downloadLatestPackage(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const out1 = yield request.get(this.resolveUrl(`/dash/apps/latestPackageInfo`))
                .set(definitions_1.NOAH_TOKEN_NAME, this.getNoahToken())
                .query({
                appKey: data.appKey,
                appVersion: data.appVersion
            });
            const body = out1.body;
            if (body.status !== 0) {
                throw new Error(out1.body.message);
            }
            const serverMd5 = body.data.md5;
            if (!serverMd5) {
                throw new Error(`未读取到server端返回的全量包md5`);
            }
            const p1 = new Promise((resolve, reject) => {
                try {
                    const saveStream = fs.createWriteStream(data.saveFilePath);
                    const url = this.resolveUrl(`/dash/apps/downloadLatestPackage`);
                    const req = request.get(url)
                        .set(definitions_1.NOAH_TOKEN_NAME, this.getNoahToken())
                        .query({
                        appKey: data.appKey,
                        appVersion: data.appVersion
                    });
                    req.pipe(saveStream);
                    console.log(`after req.pipe`);
                    saveStream.on('close', () => {
                        //检查 md5 是否匹配
                        console.log(`write stream finish called`);
                        resolve();
                    });
                    saveStream.on('error', (err) => {
                        console.error(`write stream error called`);
                        reject(err);
                    });
                }
                catch (err) {
                    reject(err);
                }
            });
            yield p1;
            console.log(`server md5: ${serverMd5}`);
            let md5 = '';
            try {
                md5 = yield fsDiff.sign.fileMd5(data.saveFilePath);
            }
            catch (err) {
                throw new Error(`计算下载后全量包的md5异常！${err.message}`);
            }
            console.log(`local md5: ${md5}`);
            if (serverMd5 !== md5) {
                throw new Error(`server端返回的md5[${serverMd5}]和本地计算的全量包md5[${md5}]不等！`);
            }
        });
    }
}
exports.default = AppService;
