"use strict";
/**
 * 登录
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inquirer = __importStar(require("inquirer"));
const env_reader_1 = __importDefault(require("../middleware/env-reader"));
const passport_1 = __importDefault(require("../service/passport"));
exports.command = 'login';
exports.desc = '登录热更新后台系统';
function builder(argv) {
    return argv.option('env', {
        type: 'string',
        desc: '环境名',
        demandOption: true,
    }).middleware([env_reader_1.default]);
}
exports.builder = builder;
function handler(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const out = yield inquirer.prompt([
            {
                type: 'input',
                name: 'userName',
                message: '请输入用户名'
            },
            {
                type: 'password',
                name: 'password',
                message: '请输入密码'
            }
        ]);
        // console.log(`user env is ${args.env} input is: ${JSON.stringify(out)}`);
        const envObj = args.$envObj;
        const passport = new passport_1.default(envObj);
        let loginResult = null;
        try {
            loginResult = yield passport.login({ userName: out.userName, password: out.password });
        }
        catch (err) {
            console.error(`登录异常！${err.message}`);
            return;
        }
        try {
            envObj.setNoahToken(loginResult.token);
            envObj.saveCredential();
        }
        catch (err) {
            console.error(`保存登录token异常！${err.message}`);
            return;
        }
        console.info(`成功登录`);
    });
}
exports.handler = handler;
