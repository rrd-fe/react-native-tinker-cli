"use strict";
/**
 * 下载某个APP，某个native版本对应的最新全量包
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inquirer = __importStar(require("inquirer"));
const path = require('path');
const fs = require('fs');
const fse = require('fs-extra');
const AdmZip = require('adm-zip');
const env_reader_1 = __importDefault(require("../middleware/env-reader"));
const index_1 = require("../definitions/index");
const app_1 = __importDefault(require("../service/app"));
const commandUtil = __importStar(require("../utils/command"));
const fs_helper_1 = __importDefault(require("../utils/fs-helper"));
exports.command = 'download';
exports.desc = '下载最新的RN全量包';
function builder(argv) {
    return argv.option('env', {
        type: 'string',
        desc: '环境名',
        demandOption: true,
    }).option('appKey', {
        type: 'string',
        desc: 'app对应的key',
        demandOption: true,
    }).option('appVersion', {
        type: 'string',
        desc: '对应的native版本号',
        demandOption: true,
    }).option('confirm', {
        type: 'boolean',
        desc: '确认发版',
    }).option('iosDest', {
        type: 'string',
        desc: 'iOS APP解压全量包的目录',
    }).option('androidAssets', {
        type: 'string',
        desc: 'Android APP解压的assets目录',
    }).option('androidRes', {
        type: 'string',
        desc: 'Android APP解压res的目录',
    }).middleware([env_reader_1.default]);
}
exports.builder = builder;
function handler(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const appService = new app_1.default(args.$envObj);
        let app = null;
        try {
            app = yield appService.getPartialAppByKey(args.appKey);
        }
        catch (err) {
            return commandUtil.fail(new Error(`查询appKey[${args.appKey}]对应的应用详情异常！${err.message}`));
        }
        if (!args.confirm) {
            //命令行没有强制确认，需要用户手动确认
            const out = yield inquirer.prompt([
                {
                    type: 'confirm',
                    name: 'isConfirm',
                    default: false,
                    message: `再次确认下载全量包信息： 环境[${args.env}] app名[${app.name}] platform[${index_1.PlatformText[app.platform]}] native版本号[${args.appVersion}]`
                }
            ]);
            if (!out.isConfirm) {
                return;
            }
        }
        if (app.platform === index_1.AppPlatform.ios) {
            if (!args.iosDest) {
                return commandUtil.fail(new Error(`iOS的APP发版，--iosDest 解压目录必传！`));
            }
            args.iosDest = path.resolve(process.cwd(), args.iosDest);
            fse.ensureDirSync(args.iosDest);
        }
        if (app.platform === index_1.AppPlatform.android) {
            if (!args.androidAssets || !args.androidRes) {
                return commandUtil.fail(new Error(`Android的APP发版，--androidAssets --androidRes 解压目录必传！`));
            }
            args.androidAssets = path.resolve(process.cwd(), args.androidAssets);
            fse.ensureDirSync(args.androidAssets);
            args.androidRes = path.resolve(process.cwd(), args.androidRes);
            fse.ensureDirSync(args.androidRes);
        }
        let localZipFile = '';
        try {
            const tempDir = yield fs_helper_1.default.generateTempDir('tinker');
            localZipFile = path.normalize(`${tempDir}${path.sep}download_package.zip`);
        }
        catch (err) {
            return commandUtil.fail(new Error(`生成临时zip包下载文件失败！${err.message}`));
        }
        console.log(`全量zip包临时下载文件路径：${localZipFile}`);
        try {
            yield appService.downloadLatestPackage({
                appKey: args.appKey,
                appVersion: args.appVersion,
                saveFilePath: localZipFile
            });
        }
        catch (err) {
            return commandUtil.fail(new Error(`下载全量包异常！${err.message}`));
        }
        try {
            switch (app.platform) {
                case index_1.AppPlatform.android:
                    yield androidHandle(app, args, localZipFile);
                    break;
                case index_1.AppPlatform.ios:
                    yield iosHandle(app, args, localZipFile);
                    break;
                default:
                    throw new Error(`不支持的APP平台：${app.platform}`);
            }
        }
        catch (err) {
            return commandUtil.fail(err);
        }
        //清除临时的zip包
        try {
            fs.unlinkSync(localZipFile);
            console.info(`临时全量包删除完成`);
        }
        catch (err) {
            console.error(`删除临时zip全量包异常！[${localZipFile}] ${err.message}`);
        }
        commandUtil.succeed(`下载全量包成功`);
    });
}
exports.handler = handler;
/**
 * iOS下，只需要把全量包解压到指定的一个目录，就O了
 * @param app
 * @param args
 * @param zipFilePath
 */
function iosHandle(app, args, zipFilePath) {
    return __awaiter(this, void 0, void 0, function* () {
        yield fs_helper_1.default.unzip2Dir(zipFilePath, args.iosDest, true);
    });
}
/**
 * Android下，图片资源和代码资源(以及后台系统生成的hot.json等文件)，需要解压到2个不同的目录下
 * 代码资源是解压到 指定的assets 目录下
 * 图片资源是解压到 指定的res 目录下，对应的 drawable drawable-xxhdpi 等子目录下
 * @param app
 * @param args
 * @param zipFilePath
 */
function androidHandle(app, args, zipFilePath) {
    return __awaiter(this, void 0, void 0, function* () {
        const zip = new AdmZip(zipFilePath);
        //先全部解压到 res 目录下，然后把代码mv 到 assets 目录
        yield fs_helper_1.default.unzip2Dir(zipFilePath, args.androidRes, true);
        const entries = zip.getEntries();
        const files = [];
        entries.forEach((zipEntry) => {
            files.push(zipEntry.entryName);
            if (!/^\/?drawable/.test(zipEntry.entryName)) {
                //move到 assets 目录
                const src = path.resolve(args.androidRes, zipEntry.entryName);
                const target = path.resolve(args.androidAssets, zipEntry.entryName);
                console.info(`从res目录移动文件(目录)到assets目录：src[${src}] target[${target}]`);
                fse.moveSync(src, target, true);
            }
        });
        const str = files.join('\n');
        const saveFilePath = path.normalize(`${args.androidAssets}/rn_files.txt`);
        fs.writeFileSync(saveFilePath, str, { encoding: 'utf8' });
    });
}
