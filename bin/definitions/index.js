"use strict";
/**
 * 各种接口、类定义
 */
Object.defineProperty(exports, "__esModule", { value: true });
const path = require('path');
const fs = require('fs');
const os = require('os');
const packageData = require('../../package.json');
//当前cli版本
exports.VERSION = packageData.version;
//登录态相关的header名
exports.NOAH_TOKEN_NAME = 'x-tinker-token';
/**
 * 当前执行的环境类
 */
class Env {
    constructor(envName) {
        this.envName = '';
        this.server = '';
        this.tinkerToken = '';
        this.envName = envName;
    }
    /**
     * 判断某个环境的配置文件是否已经存在
     * @param envName 环境名
     */
    static isEnvExist(envName) {
        const envFilePath = Env.resolveEnvFile(envName);
        return fs.existsSync(envFilePath);
    }
    static resolveEnvFile(envName) {
        return path.resolve(process.cwd(), `.tinker.${encodeURIComponent(envName)}.json`);
    }
    loadFile() {
        const filePath = Env.resolveEnvFile(this.envName);
        if (!fs.existsSync(filePath)) {
            throw new Error(`[${this.envName}]环境不存在！`);
        }
        const conf = require(filePath);
        if (!conf.server) {
            throw new Error(`环境配置文件错误！[server]字段必填`);
        }
        this.server = conf.server;
    }
    getServer() {
        return this.server;
    }
    resolveUrl(urlPath) {
        return this.server + urlPath;
    }
    setNoahToken(token) {
        this.tinkerToken = token;
    }
    getNoahToken() {
        return this.tinkerToken;
    }
    resolveCredentialFile() {
        return path.join(os.homedir(), `.tinker.${encodeURIComponent(this.envName)}.json`);
    }
    saveCredential() {
        const data = {
            version: exports.VERSION,
            token: this.tinkerToken
        };
        const filePath = this.resolveCredentialFile();
        fs.writeFileSync(filePath, JSON.stringify(data));
    }
    loadCredential() {
        const filePath = this.resolveCredentialFile();
        try {
            const exist = fs.existsSync(filePath);
            if (exist) {
                const data = require(filePath);
                this.tinkerToken = data.token;
            }
        }
        catch (err) {
            console.error(`读取登录数据异常：${err.message}`);
        }
    }
    removeCredential() {
        const filePath = this.resolveCredentialFile();
        try {
            const exist = fs.existsSync(filePath);
            if (exist) {
                fs.unlinkSync(filePath);
            }
        }
        catch (err) {
            console.error(`删除登录态异常！${err.message}`);
        }
    }
}
exports.Env = Env;
var AppPlatform;
(function (AppPlatform) {
    AppPlatform[AppPlatform["android"] = 1] = "android";
    AppPlatform[AppPlatform["ios"] = 2] = "ios";
})(AppPlatform = exports.AppPlatform || (exports.AppPlatform = {}));
exports.PlatformText = {
    [AppPlatform.android]: 'Android',
    [AppPlatform.ios]: 'iOS'
};
