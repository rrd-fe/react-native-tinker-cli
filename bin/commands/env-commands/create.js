"use strict";
/**
 * 创建一个env
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require('fs');
const definitions_1 = require("../../definitions");
exports.command = 'create';
exports.desc = '创建环境';
function builder(argv) {
    return argv.option('name', {
        desc: '要创建的环境名，请使用【字母、数字】组合',
        demandOption: true,
        type: 'string',
    }).option('server', {
        desc: '该环境对应的server信息，示例：http://test.tinker.com',
        demandOption: true,
        type: 'string',
        string: true,
    });
}
exports.builder = builder;
function handler(argv) {
    return __awaiter(this, void 0, void 0, function* () {
        if (definitions_1.Env.isEnvExist(argv.name)) {
            console.error(`已经存在相同的环境名了: ${argv.name}`);
            return;
        }
        if (!argv.server) {
            return console.error(`[server]不能为空`);
        }
        const filePath = definitions_1.Env.resolveEnvFile(argv.name);
        try {
            const conf = {
                server: argv.server
            };
            fs.writeFileSync(filePath, JSON.stringify(conf));
        }
        catch (err) {
            console.error(`写入环境配置文件失败`);
            return;
        }
        console.info(`创建环境成功`);
    });
}
exports.handler = handler;
