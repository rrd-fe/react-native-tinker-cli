"use strict";
/**
 * 用户登录、登出相关接口
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const request = __importStar(require("superagent"));
const base_1 = __importDefault(require("./base"));
const definitions_1 = require("../definitions");
class PassportService extends base_1.default {
    /**
     * 登录接口
     * @param data
     */
    login(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = this.resolveUrl(`/dash/cli/login`);
            // console.log(`login url: ${url}`);
            const res = yield request.post(url).send(data);
            const body = res.body;
            if (body.status === 0) {
                const token = res.header[definitions_1.NOAH_TOKEN_NAME];
                const userName = res.body.name;
                return {
                    token,
                    userName,
                };
            }
            else {
                throw new Error(body.message);
            }
        });
    }
}
exports.default = PassportService;
