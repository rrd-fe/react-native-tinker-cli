/**
 * 当前登录用户有权限的APP列表
 */

import * as yargs from 'yargs';
import * as inquirer from 'inquirer';
import Table from 'cli-table';
import envLoader from '../middleware/env-reader';
import { IBaseArgs, IApp } from '../definitions/index';
import AppService from '../service/app';

export const command = 'apps';
export const desc = '查看有权限的APP列表';
export function builder(argv: yargs.Argv){
    return argv.option('env', {
        type: 'string',
        desc: '环境名',
        demandOption: true,
      }).middleware([envLoader]);
}
export async function handler(args: IBaseArgs){
    const appService = new AppService(args.$envObj!);
    let result = null;
    try{
        result = await appService.relateApps();
    }catch(err){
        return console.error(`获取APP列表异常：${err.message}`);
    }

    const table = new Table({
        head: [ 'id', 'appKey', '所属平台', 'APP简介', 'git仓库地址', '打包入口脚本', '产出bundle文件名']
    });

    const arr = (<IApp[]>[]).concat(result.own, result.write);
    arr.forEach((obj) => {
        table.push([
            obj.id,
            obj.appKey,
            obj.platformName,
            obj.desc,
            obj.gitUrl,
            obj.entryFile,
            obj.bundleName
        ])
    });

    console.log(table.toString());
}