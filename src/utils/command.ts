/**
 * 命令相关的helper
 */

/**
 * 某个命令执行失败，打印错误信息，并且退出进程
 * @param err 异常的错误对象
 */
export function fail(err: Error): void{
    console.error(err.message);
    process.exit(1);
}

/**
 * 命令执行成功，打印提示信息，退出进程
 * @param msg 提示信息
 */
export function succeed(msg: string){
    console.log(msg);
    process.exit(0);
}
