/**
 * 各种接口、类定义
 */

const path = require('path');
const fs = require('fs');
const os = require('os');
import * as yargs from 'yargs';
import { internals } from 'rx';
const packageData = require('../../package.json');

//当前cli版本
export const VERSION = packageData.version;

//登录态相关的header名
export const NOAH_TOKEN_NAME = 'x-tinker-token';

//环境配置的数据结构
export interface IEnvConfig{
    server: string;
}

/**
 * 当前执行的环境类
 */
export class Env{

    /**
     * 判断某个环境的配置文件是否已经存在
     * @param envName 环境名
     */
    public static isEnvExist(envName: string): boolean{
        const envFilePath = Env.resolveEnvFile(envName);
        return fs.existsSync(envFilePath);
    }

    public static resolveEnvFile(envName: string): string{
        return path.resolve(process.cwd(), `.tinker.${encodeURIComponent(envName)}.json`);
    }

    private envName: string = '';
    private server: string = '';
    private tinkerToken: string = '';

    constructor(envName: string){
        this.envName = envName;
    }

    public loadFile(): void{
        const filePath = Env.resolveEnvFile(this.envName);
        if( ! fs.existsSync(filePath)){
            throw new Error(`[${this.envName}]环境不存在！`);
        }
        const conf: IEnvConfig = require(filePath);
        if( ! conf.server ){
            throw new Error(`环境配置文件错误！[server]字段必填`);
        }
        this.server = conf.server;
    }

    public getServer(): string{
        return this.server;
    }

    public resolveUrl(urlPath: string){
        return this.server + urlPath;
    }

    public setNoahToken(token: string){
        this.tinkerToken = token;
    }

    public getNoahToken(): string{
        return this.tinkerToken;
    }

    private resolveCredentialFile(): string{
        return path.join(os.homedir(), `.tinker.${encodeURIComponent(this.envName)}.json`);
    }

    public saveCredential(){
        const data = {
            version: VERSION,
            token: this.tinkerToken
        };
        const filePath = this.resolveCredentialFile();
        fs.writeFileSync(filePath, JSON.stringify(data));
    }

    public loadCredential(){
        const filePath = this.resolveCredentialFile();
        try{
            const exist = fs.existsSync(filePath);
            if( exist ){
                const data = require(filePath);
                this.tinkerToken = data.token;
            }
        }catch(err){
            console.error(`读取登录数据异常：${err.message}`);
        }
    }

    public removeCredential(){
        const filePath = this.resolveCredentialFile();
        try{
            const exist = fs.existsSync(filePath);
            if( exist ){
                fs.unlinkSync(filePath);
            }
        }catch(err){
            console.error(`删除登录态异常！${err.message}`);
        }
    }

}

//各种命令handler的参数的基类
export interface IBaseArgs extends yargs.Arguments{
    //当前环境名
    env?: string;
    $envObj?: Env;
}

export enum AppPlatform{
    android=1,
    ios=2
}

export const PlatformText = {
    [AppPlatform.android]: 'Android',
    [AppPlatform.ios]: 'iOS'
}

export interface IApp{
    id: number;
    appKey: string;
    name: string;
    desc: string;
    platform: AppPlatform;
    platformName: string;
    gitUrl: string;
    entryFile: string;
    bundleName: string;
    createdAt: string;
    updatedAt: string;
}