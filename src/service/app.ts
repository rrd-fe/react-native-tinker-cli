/**
 * 应用相关api
 */

import * as request from 'superagent';
const fs = require('fs');
const fsDiff = require('fs-diff');
import BaseService from './base';
import { NOAH_TOKEN_NAME, IApp, AppPlatform } from '../definitions';

export interface IRelateAppsResult{
    own: IApp[];
    write: IApp[];
}

//上传源代码方式发版的 参数
export interface ISourcePublishParam{
    //app key
    appKey: string;
    //native版本号
    appVersion: string;
    //本次发版描述
    desc: string;
    //源代码zip压缩包绝对路径
    sourceZipFile: string;
}

//本地bundle之后，上传全量包的方式发版
export interface IBundlePublishParam{
    //app key
    appKey: string;
    //native版本号
    appVersion: string;
    //本次发版描述
    desc: string;
    //bundle产出zip压缩包绝对路径
    zipFile: string;
}

export interface IPublishResult{
    //发版成功的任务ID
    taskId: number;
}

export interface IPartialApp{
    id: string;
    appKey: string;
    name: string;
    entryFile: string;
    platform: AppPlatform;
}

export interface IDownloadLatestPackageParam{
    appKey: string;
    appVersion: string;
    //下载到本地的文件绝对路径
    saveFilePath: string;
}

export default class AppService extends BaseService{

    /**
     * 获取用户有 写 权限的APP列表
     */
    async relateApps(): Promise<IRelateAppsResult>{
        const url = this.resolveUrl(`/dash/cli/relateApps`);
        const res = await request.get(url).set(NOAH_TOKEN_NAME, this.getNoahToken());
        const body = res.body;
        if(body.status === 0){
            return body.data;
        }else{
            throw new Error(body.message);
        }
    }

    /**
     * 上传源代码压缩包发版
     * @param data 
     */
    async publishBySource(data: ISourcePublishParam): Promise<IPublishResult>{
        let md5 = '';
        try{
            md5 = await fsDiff.sign.fileMd5(data.sourceZipFile);
        }catch(err){
            throw new Error(`计算源代码包的md5异常！${err.message}`);
        }
        const url = this.resolveUrl(`/dash/cli/publishBySource`);
        const res = await request.post(url)
            .set(NOAH_TOKEN_NAME, this.getNoahToken())
            .field('appKey', data.appKey)
            .field('appVersion', data.appVersion)
            .field('md5', md5)
            .field('desc', data.desc)
            .attach('sourceFile', data.sourceZipFile);
        const body = res.body;
        if(body.status === 0){
            return body.data;
        }else{
            throw new Error(body.message);
        }
    }

    /**
     * 在本地打好 bundle 之后，上传 bundle之后的全量包发版
     * @param data 
     */
    async publishByBundle(data: IBundlePublishParam): Promise<IPublishResult>{
        let md5 = '';
        try{
            md5 = await fsDiff.sign.fileMd5(data.zipFile);
        }catch(err){
            throw new Error(`计算源代码包的md5异常！${err.message}`);
        }
        const url = this.resolveUrl(`/dash/cli/publishByBundlePackage`);
        const res = await request.post(url)
            .set(NOAH_TOKEN_NAME, this.getNoahToken())
            .field('appKey', data.appKey)
            .field('appVersion', data.appVersion)
            .field('md5', md5)
            .field('desc', data.desc)
            .attach('zipFile', data.zipFile);
        const body = res.body;
        if(body.status === 0){
            return body.data;
        }else{
            throw new Error(body.message);
        }
    }

    /**
     * 根据 appKey ，获取APP的部分公开属性
     * @param appKey 创建APP时，生成的key
     */
    async getPartialAppByKey(appKey: string): Promise<IPartialApp>{
        const url = this.resolveUrl(`/dash/cli/appDetail`);
        const res = await request.get(url).set(NOAH_TOKEN_NAME, this.getNoahToken()).query({
            appKey,
        });
        const body = res.body;
        if(body.status === 0){
            return body.data;
        }else{
            throw new Error(body.message);
        }
    }

    /**
     * 下载某个APP下，某个native版本对应的最新的RN全量包
     * @param data 
     */
    async downloadLatestPackage(data: IDownloadLatestPackageParam){
        
        const out1 = await request.get( this.resolveUrl(`/dash/apps/latestPackageInfo`))
        .set(NOAH_TOKEN_NAME, this.getNoahToken())
        .query({
            appKey: data.appKey,
            appVersion: data.appVersion
        });
        const body = out1.body;

        if( body.status !== 0 ){
            throw new Error(out1.body.message);
        }

        const serverMd5 = body.data.md5;
        if( ! serverMd5 ){
            throw new Error(`未读取到server端返回的全量包md5`);
        }

        const p1 =  new Promise((resolve, reject) => {
            try{
                const saveStream = fs.createWriteStream(data.saveFilePath);
                const url = this.resolveUrl(`/dash/apps/downloadLatestPackage`);
                const req = request.get(url)
                        .set(NOAH_TOKEN_NAME, this.getNoahToken())
                        .query({
                            appKey: data.appKey,
                            appVersion: data.appVersion
                        });
                req.pipe(saveStream);
                console.log(`after req.pipe`)
                saveStream.on('close', () => {
                    //检查 md5 是否匹配
                    console.log(`write stream finish called`);
                    resolve();
                });
                saveStream.on('error', (err: Error) => {
                    console.error(`write stream error called`);
                    reject(err);
                });
            }catch(err){
                reject(err);
            }
        });
        
        await p1;
        console.log(`server md5: ${serverMd5}`);
        let md5 = '';
        try{
            md5 = await fsDiff.sign.fileMd5(data.saveFilePath);
        }catch(err){
            throw new Error(`计算下载后全量包的md5异常！${err.message}`);
        }
        console.log(`local md5: ${md5}`);
        if( serverMd5 !== md5 ){
            throw new Error(`server端返回的md5[${serverMd5}]和本地计算的全量包md5[${md5}]不等！`);
        }
    }
}