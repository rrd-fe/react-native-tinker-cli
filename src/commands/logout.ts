/**
 * 删除本地登录凭证
 */

import * as yargs from 'yargs';
import * as inquirer from 'inquirer';
import envLoader from '../middleware/env-reader';
import { IBaseArgs } from '../definitions/index';

export const command = 'logout';
export const desc = '退出登录';
export function builder(argv: yargs.Argv){
    return argv.option('env', {
        type: 'string',
        desc: '环境名',
        demandOption: true,
      }).middleware([envLoader]);
}
export async function handler(args: IBaseArgs){
    const out = <inquirer.Answers> await inquirer.prompt([
        {
            type: 'confirm',
            name: 'isConfirm',
            message: '是否确认退出登录'
        }
    ]);
    if( ! out.isConfirm ){
        return;
    }
    try{
        args.$envObj!.removeCredential();
    }catch(err){
        console.error(`删除本地登录态异常！${err.message}`);
        return;
    }
    console.info(`已退出`);
}