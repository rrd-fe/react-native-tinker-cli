/**
 * service base class
 */

import * as request from 'superagent';
import { Env } from '../definitions';

export default class BaseService{
    private env: Env;
    constructor(env: Env){
        this.env = env;
    }

    public resolveUrl(urlPath: string){
        return this.env.getServer() + urlPath;
    }

    getNoahToken(): string{
        return this.env.getNoahToken();
    }
}