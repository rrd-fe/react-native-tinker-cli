"use strict";
/**
 * 通过在本地调用  react-native bundle 命令打包产出全量包，再上传到tinker-server方式发版
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inquirer = __importStar(require("inquirer"));
const path = require('path');
const fs = require('fs');
const childProcess = require('child_process');
const fse = require('fs-extra');
const fsDiff = require('fs-diff');
const chalk_1 = __importDefault(require("chalk"));
const walk = require('ignore-walk');
const env_reader_1 = __importDefault(require("../middleware/env-reader"));
const index_1 = require("../definitions/index");
const app_1 = __importDefault(require("../service/app"));
const fs_helper_1 = __importDefault(require("../utils/fs-helper"));
const commandUtil = __importStar(require("../utils/command"));
exports.command = 'release-react';
exports.desc = '上传本地产出bundle发版';
function builder(argv) {
    return argv.option('env', {
        type: 'string',
        desc: '环境名',
        demandOption: true,
    }).option('appKey', {
        type: 'string',
        desc: '要发版的app key',
        demandOption: true,
    }).option('appVersion', {
        type: 'string',
        desc: '对应的native版本号',
        demandOption: true,
    }).option('desc', {
        type: 'string',
        desc: '本次发版的描述',
        demandOption: true,
    }).option('entryFile', {
        type: 'string',
        desc: '打包入口文件',
        demandOption: false,
    }).option('sourcemapOutput', {
        type: 'string',
        desc: '产出包的 source-map',
        demandOption: false,
    }).option('config', {
        type: 'string',
        desc: 'react-native bundle config参数',
        demandOption: false,
    }).middleware([env_reader_1.default]);
}
exports.builder = builder;
function handler(args) {
    return __awaiter(this, void 0, void 0, function* () {
        const envObj = args.$envObj;
        if (!envObj.getNoahToken()) {
            return console.error(`未登录`);
        }
        const appService = new app_1.default(envObj);
        let result = null;
        try {
            result = yield appService.relateApps();
        }
        catch (err) {
            return console.error(`获取APP列表异常：${err.message}`);
        }
        //找到对应APP的名字
        const appList = [].concat(result.own, result.write);
        const targetApp = appList.find((obj) => {
            return obj.appKey === args.appKey;
        });
        if (!targetApp) {
            return console.error(`appKey[${args.appKey}]对应的APP不存在，或者没有权限`);
        }
        const out = yield inquirer.prompt([
            {
                type: 'confirm',
                name: 'isConfirm',
                default: false,
                message: `再次确认[bundle]发版信息： 发版环境[${args.env}] app名[${targetApp.name}] native版本号[${args.appVersion}]`
            }
        ]);
        if (!out.isConfirm) {
            return;
        }
        const cwd = process.cwd();
        const tempDir = yield fs_helper_1.default.generateTempDir('tinker_release');
        let entryFile = args.entryFile;
        const platformString = index_1.PlatformText[targetApp.platform].toLocaleLowerCase();
        if (!entryFile) {
            entryFile = `index.${platformString}.js`;
            console.info(`未置顶 entryFile ，按照平台自动设置为：${entryFile}`);
        }
        try {
            console.log(`react-native bundle产出目录：${tempDir}`);
            yield runReactNativeBundle(targetApp.bundleName, false, entryFile, tempDir, platformString, args.sourcemapOutput, args.config);
        }
        catch (err) {
            return commandUtil.fail(err);
        }
        const zipDir = yield fs_helper_1.default.generateTempDir('tinker_zip');
        const zipFilePath = path.normalize(`${zipDir}${path.sep}rinker_package.zip`);
        console.log(`准备生成zip包，产出的临时zip文件路径为：${zipFilePath}`);
        try {
            yield fs_helper_1.default.zipDir(tempDir, zipFilePath);
        }
        catch (err) {
            return commandUtil.fail(err);
        }
        let publishResult = null;
        try {
            publishResult = yield appService.publishByBundle({
                zipFile: zipFilePath,
                appKey: args.appKey,
                appVersion: args.appVersion,
                desc: args.desc,
            });
        }
        catch (err) {
            return console.error(`调用发布接口异常！${err.message}`);
        }
        //清除中间临时文件
        try {
            fse.removeSync(tempDir);
            fse.removeSync(zipDir);
            fse.removeSync(zipFilePath);
        }
        catch (err) {
            console.warn(`清除临时文件失败 \n 错误信息：${err.message}`);
        }
        console.info(`成功发版！本次发版任务：${publishResult.taskId}`);
        console.info(`本次发版任务详情URL：\n`);
        const taskUrl = args.$envObj.resolveUrl(`/dash/tasks/detail?appId=${targetApp.id}&taskId=${publishResult.taskId}`);
        console.info(`${taskUrl}\n`);
    });
}
exports.handler = handler;
/**
 * 代码来自MS的 code-push:
 * https://github.com/Microsoft/code-push/blob/064d70ddaff16b131836ece3723505e01844b5fa/cli/script/command-executor.ts#L1442
 * @param bundleName
 * @param development
 * @param entryFile
 * @param outputFolder
 * @param platform
 * @param sourcemapOutput
 * @param config
 */
function runReactNativeBundle(bundleName, development, entryFile, outputFolder, platform, sourcemapOutput, config) {
    let reactNativeBundleArgs = [];
    Array.prototype.push.apply(reactNativeBundleArgs, [
        path.join("node_modules", "react-native", "local-cli", "cli.js"), "bundle",
        "--assets-dest", outputFolder,
        "--bundle-output", path.join(outputFolder, bundleName),
        "--dev", development,
        "--entry-file", entryFile,
        "--platform", platform,
    ]);
    if (sourcemapOutput) {
        reactNativeBundleArgs.push("--sourcemap-output", sourcemapOutput);
    }
    if (config) {
        reactNativeBundleArgs.push("--config", config);
    }
    console.log(chalk_1.default.cyan("Running \"react-native bundle\" command:\n"));
    var reactNativeBundleProcess = childProcess.spawn("node", reactNativeBundleArgs);
    console.log(`node ${reactNativeBundleArgs.join(" ")}`);
    return new Promise((resolve, reject) => {
        reactNativeBundleProcess.stdout.on("data", (data) => {
            console.log(data.toString().trim());
        });
        reactNativeBundleProcess.stderr.on("data", (data) => {
            console.error(data.toString().trim());
        });
        reactNativeBundleProcess.on("close", (exitCode) => {
            if (exitCode) {
                return reject(new Error(`"react-native bundle" command exited with code ${exitCode}.`));
            }
            resolve();
        });
    });
}
