#!/usr/bin/env node
/**
 * 入口
 */

//关闭一些node警告信息
process.emitWarning = function(){};

import * as yargs from "yargs";
const packageJson = require("../package.json");

const argv = yargs.commandDir('commands')
  .demandCommand()
  .help()
  .argv
