"use strict";
/**
 * 命令相关的helper
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 某个命令执行失败，打印错误信息，并且退出进程
 * @param err 异常的错误对象
 */
function fail(err) {
    console.error(err.message);
    process.exit(1);
}
exports.fail = fail;
/**
 * 命令执行成功，打印提示信息，退出进程
 * @param msg 提示信息
 */
function succeed(msg) {
    console.log(msg);
    process.exit(0);
}
exports.succeed = succeed;
