/**
 * 登录
 */

import * as yargs from 'yargs';
import * as inquirer from 'inquirer';
import envLoader from '../middleware/env-reader';
import { IBaseArgs } from '../definitions/index';
import PassportService from '../service/passport';

export const command = 'login';
export const desc = '登录热更新后台系统';
export function builder(argv: yargs.Argv){
    return argv.option('env', {
        type: 'string',
        desc: '环境名',
        demandOption: true,
      }).middleware([envLoader]);
}
export async function handler(args: IBaseArgs){
    const out = <inquirer.Answers> await inquirer.prompt([
        {
            type: 'input',
            name: 'userName',
            message: '请输入用户名'
        },
        {
            type: 'password',
            name: 'password',
            message: '请输入密码'
        }
    ]);
    // console.log(`user env is ${args.env} input is: ${JSON.stringify(out)}`);
    const envObj = args.$envObj!;
    const passport = new PassportService(envObj);
    let loginResult = null;
    try{
        loginResult = await passport.login({userName: out.userName, password: out.password});
    }catch(err){
        console.error(`登录异常！${err.message}`);
        return;
    }

    try{
        envObj.setNoahToken(loginResult.token);
        envObj.saveCredential();
    }catch(err){
        console.error(`保存登录token异常！${err.message}`);
        return;
    }
    console.info(`成功登录`);
}